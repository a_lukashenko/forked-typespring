import { InjectionToken } from '../dist';

export const TEST_TOKEN1 = new InjectionToken('TEST1');
export const TEST_TOKEN2 = new InjectionToken('TEST2');
export const TEST_TOKEN3 = new InjectionToken('TEST3');