import 'reflect-metadata';
import { Type } from './type';

//**********************************************//

export interface TypeProvider extends Type<any> {
}

export interface ValueProvider {
    provide: InjectionToken;
    value: any;
}

export type Provider = TypeProvider | ValueProvider;

//**********************************************//

export class InjectionToken {
    readonly _key;

    constructor(key: string) {
        this._key = Symbol(key);
    }

    get key(): string {
        return this._key;
    }
}

export const SERVER_PROVIDER = new InjectionToken('SERVER');
export const APP_PROVIDER = new InjectionToken('APP');
export const SERVICE_PROVIDER = new InjectionToken('SERVICE_PROVIDER');

//**********************************************//

export function Inject(token: InjectionToken) {
    return (target: Object, propertyKey: string | symbol, parameterIndex: number) => {
        Reflect.defineMetadata(token.key, parameterIndex, target);
    }
}

export function Injectable() {
    return (target) => {
        Reflect.defineMetadata(SERVICE_PROVIDER, Injector.injectorIndexer++, target);
    };
}

//**********************************************//

export class Injector {
    static injectorIndexer: number = 0;
    static instances: Map<string, any> = new Map();

    static register(target: Provider): void {
        if (typeof target === 'function') {
            const registered = Injector.resolve(target);
            if (registered) {
                throw new Error(`[DI ERROR]: service with name ${target.name} already registered!`);
            }
            const tokens = Reflect.getMetadata('design:paramtypes', target) || [];
            const metadataKeys = Reflect.getOwnMetadataKeys(target) || [];
            const metadata = metadataKeys.map(el => {
                const value = Injector.instances.get(el);
                if (value == null) return;
                const index = Reflect.getMetadata(el, target);
                if (index == null) return;
                return {index: index, value: value};
            }).filter(el => el != null);
            const injections = tokens.map((token, index) => {
                if (metadata.length) {
                    const m = metadata.find(m => m.index === index);
                    if (m) return m.value;
                }
                const resolved = Injector.resolve<any>(token);
                return resolved;
            });
            Injector.instances.set(target.name, new target(...injections));
        }
        if (typeof target === 'object') {
            Injector.instances.set(target.provide.key, target.value);
        }
    }

    static resolve<T>(target): T {
        const name = target.name;
        return Injector.instances.get(name);
    }
}


