import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import { createServer } from 'http';
import * as fileUpload from 'express-fileupload';
import 'reflect-metadata';
import { Express } from 'express';
import * as http from 'http';
import { Logger } from 'winston';
import { ILoggerConfig, LoggerResolver } from './logger';
import { APP_PROVIDER, Injector, Provider, SERVER_PROVIDER, SERVICE_PROVIDER } from './di';
import { RESTController } from './controller';
import { Type } from './type';

export function bootstrap<T>(server): T & { app: Express, server: http.Server } {
    return new server();
}

export interface IServerInitializer {
    init();
}

export interface IServerOptions {
    port: number | string;
    initializer: Type<IServerInitializer>;
    cors?: boolean;
    whitelistedDomains?: string[],
    providers?: Provider[];
    controllers?: RESTController[];
    staticDir?: string;
    publicDir?: string;
    loggerConfig?: ILoggerConfig;
    bodySize?: number;
}

export function Server(options: IServerOptions) {
    return <T extends { new(...args: any[]): {} }>(constructor: T) => {
        return class extends constructor {
            logger: Logger;
            app: Express = express();
            server: http.Server = createServer(this.app);

            constructor(...args: any[]) {
                super(...args);
                this.run();
            }

            run(): void {
                this.initProviders([
                    {provide: APP_PROVIDER, value: this.app},
                    {provide: SERVER_PROVIDER, value: this.server},
                    ...options.providers,
                    options.initializer]);

                LoggerResolver.register('SYSTEM', this, 'logger');
                LoggerResolver.resolve(options.loggerConfig);

                const staticDir = options.staticDir;
                if (staticDir) {
                    this.app.use('/static', express.static(staticDir));
                }

                const publicDir = options.publicDir;
                if (publicDir) {
                    this.app.use('/public', express.static(publicDir));
                }

                const bodySize = options.bodySize ? `${options.bodySize}mb` : '1mb';
                this.app.use(bodyParser.json({limit: bodySize}));
                this.app.use(bodyParser.urlencoded({
                    extended: false,
                    limit: bodySize,
                }));

                const whitelistedDomains = options.whitelistedDomains;
                if (options.cors || !whitelistedDomains || !whitelistedDomains.length) {
                    this.app.use(cors());
                } else if (whitelistedDomains && whitelistedDomains.length) {
                    const corsOptions = {
                        origin: function (origin, callback) {
                            if (whitelistedDomains.indexOf(origin) !== -1) {
                                callback(null, true)
                            } else {
                                callback(new Error(`Domain ${origin} is not allowed by CORS`))
                            }
                        },
                    };
                    this.app.use(cors(corsOptions));
                }
                this.app.use(fileUpload({preserveExtension: true}));
                this.initControllers(options.controllers);

                this.server.listen(options.port, () => {
                    this.logger.info(`Node Express server listening on http://localhost:${options.port}`);
                    const initializer: IServerInitializer = Injector.resolve(options.initializer);
                    initializer.init();
                });
            }

            initControllers(controllers: RESTController[]): void {
                const instances = controllers.map(c => {
                    const tokens = Reflect.getMetadata('design:paramtypes', c) || [];
                    const injections = tokens.map(token => Injector.resolve<RESTController>(token));
                    return new c(...injections);
                });
                instances.forEach(instance => {
                    const requests = instance['__requests'];
                    if (!requests.length) return;
                    requests.forEach(r => {
                        this.app.use(`/api${instance.path}`, instance[r](instance.middleware));
                    });
                });
            }

            initProviders(providers: Provider[]): void {
                const valueProviders = providers.filter(p => typeof p === 'object');
                const typeProviders = providers.filter(p => typeof p === 'function');
                valueProviders.forEach(provider => {
                    Injector.register(provider);
                });
                typeProviders.sort((p1, p2) => {
                    const p1Index = Reflect.getMetadata(SERVICE_PROVIDER, p1);
                    const p2Index = Reflect.getMetadata(SERVICE_PROVIDER, p2);
                    if (p1Index > p2Index) return 1;
                    if (p1Index < p2Index) return -1;
                    return 0;
                }).forEach(provider => {
                    Injector.register(provider);
                });
            }
        };
    };
}
