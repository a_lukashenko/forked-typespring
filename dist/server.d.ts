/// <reference types="node" />
import * as express from 'express';
import 'reflect-metadata';
import { Express } from 'express';
import * as http from 'http';
import { Logger } from 'winston';
import { ILoggerConfig } from './logger';
import { Provider } from './di';
import { RESTController } from './controller';
import { Type } from './type';
export declare function bootstrap<T>(server: any): T & {
    app: Express;
    server: http.Server;
};
export interface IServerInitializer {
    init(): any;
}
export interface IServerOptions {
    port: number | string;
    initializer: Type<IServerInitializer>;
    cors?: boolean;
    whitelistedDomains?: string[];
    providers?: Provider[];
    controllers?: RESTController[];
    staticDir?: string;
    publicDir?: string;
    loggerConfig?: ILoggerConfig;
    bodySize?: number;
}
export declare function Server(options: IServerOptions): <T extends new (...args: any[]) => {}>(constructor: T) => {
    new (...args: any[]): {
        logger: Logger;
        app: express.Express;
        server: http.Server;
        run(): void;
        initControllers(controllers: RESTController[]): void;
        initProviders(providers: Provider[]): void;
    };
} & T;
