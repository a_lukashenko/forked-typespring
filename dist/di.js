"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
//**********************************************//
var InjectionToken = /** @class */ (function () {
    function InjectionToken(key) {
        this._key = Symbol(key);
    }
    Object.defineProperty(InjectionToken.prototype, "key", {
        get: function () {
            return this._key;
        },
        enumerable: true,
        configurable: true
    });
    return InjectionToken;
}());
exports.InjectionToken = InjectionToken;
exports.SERVER_PROVIDER = new InjectionToken('SERVER');
exports.APP_PROVIDER = new InjectionToken('APP');
exports.SERVICE_PROVIDER = new InjectionToken('SERVICE_PROVIDER');
//**********************************************//
function Inject(token) {
    return function (target, propertyKey, parameterIndex) {
        Reflect.defineMetadata(token.key, parameterIndex, target);
    };
}
exports.Inject = Inject;
function Injectable() {
    return function (target) {
        Reflect.defineMetadata(exports.SERVICE_PROVIDER, Injector.injectorIndexer++, target);
    };
}
exports.Injectable = Injectable;
//**********************************************//
var Injector = /** @class */ (function () {
    function Injector() {
    }
    Injector.register = function (target) {
        if (typeof target === 'function') {
            var registered = Injector.resolve(target);
            if (registered) {
                throw new Error("[DI ERROR]: service with name " + target.name + " already registered!");
            }
            var tokens = Reflect.getMetadata('design:paramtypes', target) || [];
            var metadataKeys = Reflect.getOwnMetadataKeys(target) || [];
            var metadata_1 = metadataKeys.map(function (el) {
                var value = Injector.instances.get(el);
                if (value == null)
                    return;
                var index = Reflect.getMetadata(el, target);
                if (index == null)
                    return;
                return { index: index, value: value };
            }).filter(function (el) { return el != null; });
            var injections = tokens.map(function (token, index) {
                if (metadata_1.length) {
                    var m = metadata_1.find(function (m) { return m.index === index; });
                    if (m)
                        return m.value;
                }
                var resolved = Injector.resolve(token);
                return resolved;
            });
            Injector.instances.set(target.name, new (target.bind.apply(target, [void 0].concat(injections)))());
        }
        if (typeof target === 'object') {
            Injector.instances.set(target.provide.key, target.value);
        }
    };
    Injector.resolve = function (target) {
        var name = target.name;
        return Injector.instances.get(name);
    };
    Injector.injectorIndexer = 0;
    Injector.instances = new Map();
    return Injector;
}());
exports.Injector = Injector;
//# sourceMappingURL=di.js.map