export declare enum LogLevel {
    Silly = "silly",
    Verbose = "verbose",
    Debug = "debug",
    Info = "info",
    Warn = "warn",
    Error = "error"
}
export interface ILoggerConfig {
    disabled?: boolean;
    console?: {
        disabled?: boolean;
        level: LogLevel | string;
    };
    file?: {
        disabled?: boolean;
        name: string;
        level: LogLevel | string;
    };
}
export declare function Logger(label: string): (target: any, key: any) => void;
export declare class LoggerResolver {
    static instances: {
        label: string;
        target: any;
        key: string;
    }[];
    static register(label: string, target: any, key: string): void;
    static resolve(config: ILoggerConfig): void;
}
