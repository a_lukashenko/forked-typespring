import { MongoClient, Collection, DbCollectionOptions } from 'mongodb';
export interface IMongoConfig {
    url: string;
}
export declare class MongoService {
    private logger;
    private _client;
    readonly client: MongoClient;
    connect(config: IMongoConfig): Promise<MongoClient>;
    collection(colName: string): Collection;
    createCollection(colName: string, options?: DbCollectionOptions): Promise<Collection<any>>;
    removeCollection(colName: string): Promise<void>;
    terminate(): void;
}
export declare function Transaction(): (target: any, propertyKey: string, descriptor: PropertyDescriptor) => PropertyDescriptor;
